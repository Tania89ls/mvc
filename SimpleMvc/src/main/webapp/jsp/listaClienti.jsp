<%@ page language="java" contentType="text/html; charset=UTF-8" import="java.util.*, it.epicode.model.entities.*"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lista clienti</title>
</head>
<body>

<%-- 
// 	List<Cliente> listaClienti = (List<Cliente>)request.getAttribute("listaClienti");
// 	//Object x = (Object)listaClienti;
// 	//List<Cliente> y = (List<Cliente>)x;
// 	for(Cliente c : listaClienti){
// 		out.print(c.getId() + " " + c.getNome() + " " + c.getCognome() + "<br>");
// 	} --%>
<table>
<tr>
	<th>id cliente </th>
	<th> nome </th>
	<th> cognome </th>
</tr>
<c:forEach items="${listaClienti}" var="c">
<tr>
<td>${c.id}</td>
<td>${c.nome}</td>
<td>${c.cognome}</td>
</tr>
</c:forEach>
</table>


</body>
</html>