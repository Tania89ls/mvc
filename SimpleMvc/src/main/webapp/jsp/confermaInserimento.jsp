<%@ page language="java" contentType="text/html; charset=UTF-8" import = "it.epicode.model.entities.*"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Conferma inserimento</title>
</head>
<body>
  

<%
	Cliente c = (Cliente)request.getAttribute("cliente");

%>


Inserito nuovo cliente.

Id: <%= c.getId() %> <br>
nome: <%= c.getNome() %><br>
cognome: <%= c.getCognome()%><br>


<jsp:useBean id="cliente" scope ="request" class ="it.epicode.model.entities.Cliente"/>
Id cliente: <jsp:getProperty property="id" name="cliente"/><br>
Nome: <jsp:getProperty property="nome" name="cliente"/><br>
Cognome: <jsp:getProperty property="cognome" name="cliente"/><br><br>


Id cliente: <c:out value="${cliente.id}"></c:out><br>
Nome: <c:out value="${cliente.nome}"></c:out><br>
Cognome: <c:out value="${cliente.cognome}"></c:out>

</body>
</html>