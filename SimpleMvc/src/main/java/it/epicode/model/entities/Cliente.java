package it.epicode.model.entities;

public class Cliente {
	
	private long id;
	private String nome;
	private String cognome;
	
	public Cliente(long id, String nome, String cognome) {
		this.id = id;
		this.nome = nome;
		this.cognome = cognome;
	}
	
	

	public Cliente() {
	}



	public long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getCognome() {
		return cognome;
	}
	
	

}
