package it.epicode.model.persistence;

import java.sql.SQLException;
import java.util.List;

import it.epicode.model.entities.Cliente;

public interface AbstractClienteDAO {

	List<Cliente> getAll() throws SQLException;
	
	Cliente salva(Cliente c) throws SQLException;
	
	List<Cliente> getByCognomeLike(String part)throws SQLException;
	
}
