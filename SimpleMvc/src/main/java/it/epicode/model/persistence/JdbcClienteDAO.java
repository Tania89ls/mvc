package it.epicode.model.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import it.epicode.model.entities.Cliente;

public class JdbcClienteDAO implements AbstractClienteDAO{
	
	public static final String SELECT_ALL_CLIENTS = "select id, nome, cognome from clienti";
	public static final String SAVE_CLIENT = "insert into clienti (id, nome, cognome) values (?,?,?)";
	public static final String CLIENT_BY_LIKE = "select id, nome, cognome from clienti where cognome like ?";
	
	
	
	@Override
	public List<Cliente> getAll() throws SQLException {
		try (Connection c = JdbcUtils.createConnection(); 
			Statement st = c.createStatement(); 
			ResultSet rs = st.executeQuery(SELECT_ALL_CLIENTS)){
			
			List<Cliente> result = clientiFromResultSet(rs);
				
			return result;
		}
	}

	@Override
	public Cliente salva(Cliente c) throws SQLException {
		try (Connection con = JdbcUtils.createConnection(); 
				PreparedStatement ps = con.prepareStatement(SAVE_CLIENT);) {
			ps.setLong(1, c.getId());
			ps.setString(2, c.getNome());
			ps.setString(3, c.getCognome());
			ps.executeUpdate();
		}
		return c;
	}

	@Override
	public List<Cliente> getByCognomeLike(String part) throws SQLException {
		try (Connection c = JdbcUtils.createConnection(); 
				PreparedStatement ps  = c.prepareStatement(CLIENT_BY_LIKE); ){
			
			ps.setString(1, "%"+part+"%");
			
			
			try(ResultSet rs = ps.executeQuery()){
				List<Cliente> result = clientiFromResultSet(rs);
				
				return result;
			}
			
			
		}
		
	}
	
	private List<Cliente> clientiFromResultSet(ResultSet rs) throws SQLException{
		
		List<Cliente> listaClienti = new ArrayList<>();
	
		while(rs.next()) {
			listaClienti.add(new Cliente(rs.getLong("id"), rs.getString("nome"), rs.getString("cognome")));
		}
		
		return listaClienti;
	}

}
