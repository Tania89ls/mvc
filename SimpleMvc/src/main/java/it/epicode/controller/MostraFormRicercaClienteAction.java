package it.epicode.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MostraFormRicercaClienteAction implements Action {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws SQLException {
		return "forms/ricercaCliente.html";
	}

}
