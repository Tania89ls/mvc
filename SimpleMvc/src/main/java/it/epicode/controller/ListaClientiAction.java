package it.epicode.controller;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.epicode.model.entities.Cliente;
import it.epicode.model.persistence.AbstractClienteDAO;

public class ListaClientiAction implements Action{

	private AbstractClienteDAO clienteDAO;
	
	//iniezione della dipendenza
	public ListaClientiAction(AbstractClienteDAO clienteDAO) {
		this.clienteDAO = clienteDAO;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws SQLException {
		
		List<Cliente> result = clienteDAO.getAll();
		request.setAttribute("listaClienti", result);
		
		return "jsp/listaClienti.jsp";
	}

}
