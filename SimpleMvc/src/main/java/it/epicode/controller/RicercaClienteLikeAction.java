package it.epicode.controller;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.epicode.model.entities.Cliente;
import it.epicode.model.persistence.AbstractClienteDAO;

public class RicercaClienteLikeAction implements Action {

	AbstractClienteDAO clienteDao;

	public RicercaClienteLikeAction(AbstractClienteDAO clienteDao) {
		this.clienteDao = clienteDao;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws SQLException {
		
		String part = request.getParameter("cognome");
		
		List<Cliente> result = clienteDao.getByCognomeLike(part);
		request.setAttribute("listaClienti", result);
		
		return "jsp/listaClienti.jsp";
	}

}
