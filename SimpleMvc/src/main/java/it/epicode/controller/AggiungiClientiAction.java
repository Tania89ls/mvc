package it.epicode.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.epicode.model.entities.Cliente;
import it.epicode.model.persistence.AbstractClienteDAO;

public class AggiungiClientiAction implements Action{
	
	private AbstractClienteDAO clienteDAO;

	public AggiungiClientiAction(AbstractClienteDAO aggiungiCliente) {
		this.clienteDAO = aggiungiCliente;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws SQLException {
		String id = request.getParameter("id");
		String nome = request.getParameter("nome");
		String cognome = request.getParameter("cognome");
		
		Cliente cli = new Cliente(Integer.parseInt(id), nome, cognome);
		
		Cliente c = clienteDAO.salva(cli);
		request.setAttribute("cliente", c);

		return "jsp/confermaInserimento.jsp";
	}

}
