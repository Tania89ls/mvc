package it.epicode.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.epicode.model.persistence.JdbcClienteDAO;


@WebServlet("*.do")
public class ApplicationController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	static {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String actionString = request.getServletPath();
		int pos = actionString.lastIndexOf("/");
		actionString = actionString.substring(pos + 1, actionString.length()-3);
		Action action = null;
		switch (actionString) {
		case "listaClienti":
			action = new ListaClientiAction(new JdbcClienteDAO());
			break;
		case "aggiungiCliente":
			action = new AggiungiClientiAction(new JdbcClienteDAO());
			break;
		case "mostraFormAggiungiCliente":
			action = new MostraFormAggiungiClienteAction();
			break;
		case "mostraFormRicercaCliente":
			action = new MostraFormRicercaClienteAction();
			break;
		case "ricercaCliente":
			action = new RicercaClienteLikeAction(new JdbcClienteDAO());
			break;
		default: 
			action = new NotSupportedAction(); 
			break;
		}
		String viewName = null;
		try {
			viewName = action.execute(request, response);
			RequestDispatcher dispatcher = request.getRequestDispatcher(viewName);
			dispatcher.forward(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	

}
